<?php

/**
 * This sample demonstrates how to log into a remote Inxmail Server. After login,
 * it shows all mailing lists, the number of recipients contained and which features
 * are available.
 * 
 * @author bgn Inxmail GmbH
 */
class Inx_Demo_List
{
	/**
	 * Connected session
	 *
	 * @var Inx_Api_Session
	 */
	private $_oSession;
	
	
	public function __construct(  )
	{
		
	}
	
	
	/**
	 * Run the demonstration
	 * 
	 * @throws Exception
	 */
	public function runDemo(Inx_Api_Session $oSession)
	{
		$this->_oSession = $oSession;
		
		$this->showListDemo();
	}
	

	/**
	 * Demonstrates how to browse through all lists and display list information
	 * 
	 * @param session          connected session
	 * @throws DataException
	 * @throws SelectException 
	 */
	private function showListDemo()
	{
		//SimpleDateFormat sdf = new SimpleDateFormat();
		$rm = $this->_oSession->createRecipientContext();
		$lm = $this->_oSession->getListContextManager();
		$rs = $lm->selectAll();

		for( $i = 0; $i < $rs->size(); $i++ )
		{
			$lc = $rs->get( $i );
			echo "List ID : " . $lc->getId() . "\n";

			//
			// List type is denoted by the interfaces a list implements
			//
			
			echo "\tType          : ";				
			if( $lc instanceof Inx_Api_List_AdminListContext )
				echo "Administration list\n";
			else if( $lc instanceof Inx_Api_List_SystemListContext )
				echo "System list\n";
			else if( $lc instanceof Inx_Api_List_FilterListContext )						
				echo "Dynamic list\n";
			else
				echo "Standard list\n";

			//
			// Show standard values
			//
			
			echo "\tName          : " . $lc->getName() . "\n";
			echo "\tDescription   : " . $lc->getDescription() . "\n";
			echo "\tCreation Date : "
				. $lc->getCreationDatetime() . "\n";
			
			//
			// All lists except the administration list have a recipients. Show the number
			// of recipients:
			//
			
			if( !($lc instanceof Inx_Api_List_AdminListContext) )
			{
				$rrs = $rm->select( $lc, null, null, $rm->getMetaData()->getEmailAttribute(), Inx_Api_Order::ASC );
				echo "\tList Size     : " . $rrs->getRowCount() . " recipients.\n";
			}

			//
			// Print the availablity and activation state of selected features
			//
			
			$this->printFeatureState( "Subscription ", $lc, Inx_Api_Features::SUBSCRIPTION_FEATURE_ID );
			$this->printFeatureState( "Mailing      ", $lc, Inx_Api_Features::MAILING_FEATURE_ID );

		}

		$rs->close();
	}
	
	
	/**
	 * Prints the state of a feature
	 * 
	 * @param title      Title to be printed
	 * @param $lc         list context
	 * @param featureId  feature id
	 */
	private function printFeatureState( $sTitle, Inx_Api_List_ListContext $lc, $iFeatureId )
	{
		echo "\t" . $sTitle . " : ";
		try
		{
			if( $lc->isFeatureEnabled( $iFeatureId ) )
				echo "activated \n";
			else
				echo "not activated \n";
		}
		catch( Inx_Api_FeatureNotAvailableException $e )
		{
			echo "(not available) \n";
		}
		catch( Exception $e )
		{
			echo $e->getMessage();
			echo $e->getTraceAsString();
		}
	}

	
	public function main( $args )
	{
		/* // For use with a proxy server:
		Inx_Api_Session::setProperty( "http.proxyHost", "- your proxy server IP address" );
		Inx_Api_Session::setProperty( "http.proxyPort", "8080" );
		Inx_Api_Session::setProperty( "http.proxyUser", "test" );
		Inx_Api_Session::setProperty( "http.proxyPassword", "test" );
		*/

		if( sizeof($args) != 3 )
		{
			echo "Missing parameters: <server> <username> <password> \n";
			echo "           Example: http://127.0.0.1/inxmail supervisor inxmail\n";
			throw new Inx_Api_IllegalArgumentException( "Not enough parameters." );
		}
		
		echo "Logging in to " . $args[0] . "... \n";
		$s = Inx_Api_Session::createRemoteSession( $args[0], $args[1], $args[2] );
		
		$this->runDemo($s);
		
		if(! empty($s))
			$s->close();
	}
}