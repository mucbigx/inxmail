<?php


/**
 * This sample demonstrates how to create and send a mailing.
 * 
 * @author bgn Inxmail GmbH
 */
class Inx_Demo_Mailing
{
	/** Subject of the created mailing */
	const SUBJECT = "MailingSample [%date] - [%time]";
	
	/** Content of the created mailing */
	const CONTENT = "Hello!\n\n[%header(X-Sample,MailingDemo)]this mail has been sent to '[email]' by MailingSample on [%date].";
		
	/**
	 * Connected session
	 *
	 * @var Inx_Api_Session
	 */
	protected $_oSession;
	
	/** 
	 * Name of the mailing list 
	 *
	 * @var Inx_Api_List_ListContext
	 */
	protected $_oMailingList;
	
	/** 
	 * Mailing manager 
	 * 
	 * @var Inx_Api_Mailing_MailingManager
	 */
	protected $_oMailingMgr;
	
	/** 
	 * Id of a test recipient
	 * 
	 * @var integer 
	 */
	protected $testRecipientId = -1;
	
	/**
	 * Create a new mailing sample. 
	 * 
	 * @param session          connected Inxmail session
	 * @param mailingListName  name of existing mailing list
	 * @throws Exception
	 */
	public function __construct(  )
	{
	
	}
	
	protected function _init( $sEmailAddress, Inx_Api_Session $oSession, $sMailingListName )
	{
		$this->_oSession = $oSession;
		$lm = $oSession->getListContextManager();
		$this->_oMailingMgr = $oSession->getMailingManager();
		$this->_oMailingList = $lm->findByName( $sMailingListName );
		
		if( $this->_oMailingList==null )
			throw new Inx_Api_IllegalArgumentException( "Mailing list '" . $sMailingListName . "' not found!" );
		else
			echo "Found Mailing list '" . $sMailingListName . "' (id:" . $this->_oMailingList->getId() . ") \n";
	}
	
	/**
	 * Run the demonstration
	 * 
	 * @throws Exception
	 */
	public function runDemo( $sEmailAddress, Inx_Api_Session $oSession, $sMailingListName )
	{
		$this->_init($sEmailAddress, $oSession, $sMailingListName);
		$mailing = $this->createMailingDemo( self::CONTENT );
		$this->sendTestMailDemo( $mailing, $sEmailAddress, $this->locateTestRecipient() );
		$this->previewMailingDemo( $mailing, $this->locateTestRecipient() );
	}
	

	/**
	 * Demonstrates how to create a new plain text mailing
	 * 
	 * @throws SelectException 
	 * @throws UpdateException
	 * @throws DataException
	 */
	protected function createMailingDemo( $content )
	{
		echo "Creating mailing '" . self::SUBJECT . "'...\n";
		
		// Create a new, empty mailing
		$mailing = $this->_oMailingMgr->createMailing( $this->_oMailingList );
		$mailing->updateSubject( self::SUBJECT );
		
		// Set its content
		$mailing->setContentHandler( 'Inx_Api_Mailing_PlainTextContentHandler' );
		$contentHandler = $mailing->getContentHandler();
		$contentHandler->updateContent( $content );

		$mailing->commitUpdate();
		
		echo "ok.\n";

		return $mailing;
	}

	
	/**
	 * Sends a test mail to the address "emailAddess"
	 * 
	 * @param mailing
	 * @param emailAddress
	 * @return
	 * @throws SelectException
	 * @throws SendException
	 * @throws MailingStateException
	 * @throws DataException
	 */
	protected function sendTestMailDemo( $mailing, $sToEmailAddress, $iRecipientId )
	{
		echo "Sending test mailing...";
		
		// Allways approve the mailing, otherwise it cannot be sent.
		echo "approving..."; 
		$mailing->approve();
		echo "sending to '$sToEmailAddress'...";
//		try {
			$mailing->sendTestMail( $sToEmailAddress, $iRecipientId );	
//		} catch(Exception $e) {
//			echo $e->getMessage();
//		}
		
		
		echo "ok.\nTest mail sent to address '$sToEmailAddress'\n";
		
		return $mailing;
	}
	
	
	/**
	 * Locate a recipient whose data will be used 
	 * 
	 * @return
	 * @throws SelectException
	 * @throws DataException
	 */
	protected function locateTestRecipient()
	{
		if( $this->testRecipientId!=-1 )
			return $this->testRecipientId;
		
		echo "Recipient lookup... \n";
		$rm = $this->_oSession->createRecipientContext();
		$rs = $rm->select();
		$rs->next();
		
		if (! $rs->getRowCount()) {
			throw new Exception('No recipients in list.');
		}
		$this->testRecipientId = $rs->getId();
		echo "ok.\n";
		
		return $this->testRecipientId;
	}
	
	
	/**
	 * Displays a mail preview
	 * 
	 * @param mailing
	 * @param recipientId
	 */
	protected function previewMailingDemo( $mailing, $recipientId )
	{
		$renderer = $this->_oSession->getMailingManager()->createRenderer();
		// First step: Parse the mailing
		try
		{
			$renderer->parse( $mailing->getId(), Inx_Api_Mail_MailingRenderer::BUILD_MODE_PREVIEW );
		}
		catch( Inx_Api_Mail_ParseException $e )
		{
			echo $e->getTraceAsString();
		} 

		// Second step: Build it!
		/**
		 * @var Inx_Api_Mail_MailContent
		 */
		$content = null;
		try
		{
			$content = $renderer->build( $recipientId );
		}
		catch( Inx_Api_Mail_BuildException $e )
		{
			echo $e->getTraceAsString();
			return;
		}
		
		// Now the content can be accessed:
		echo "From: " . $content->getSenderAddress() . "\n";
		echo "To: " . $content->getRecipientAddress() . "\n";
		echo "Reply-To: " . $content->getReplyToAddress() . "\n";
		echo "Additional Mail-Headers: " . print_r( $content->getHeader(), true) . "\n";
		echo "Content:\n" . $content->getPlainText() . "\n";
	}

	
	public function main( $args )
	{
		/* // For use with a proxy server:
		Inx_Api_Session::setProperty( "http.proxyHost", "- your proxy server IP address" );
		Inx_Api_Session::setProperty( "http.proxyPort", "8080" );
		Inx_Api_Session::setProperty( "http.proxyUser", "test" );
		Inx_Api_Session::setProperty( "http.proxyPassword", "test" );
		*/

		if( sizeof($args) != 5 )
		{
			echo 
				"Demonstrates creation and sending of mailings. To run this demo, please create\n" .
				"a standard mailing list. You need to have at least one recipient in the system\n" .
				"list, whose data is used for personalization. The created mailing will be sent\n" .
				"to an email address passed as parameter to this demo.\n\n" .
			    "Parameters: <server> <user> <password> <mailingList> <your email address>\n" .
			    "           Example: http://127.0.0.1/inxmail supervisor inxmail TestList name@company.com\n";
			throw new Inx_Api_IllegalArgumentException( "Not enough parameters" );
		}
		
		echo "Logging in to " . $args[0] . "...\n";
		$s = Inx_Api_Session::createRemoteSession( $args[0], $args[1], $args[2] );
		
		$this->runDemo($args[4], $s, $args[3]);
		
		if( $s != null )
				$s->close();
	}
}