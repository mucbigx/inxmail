<?php


/**
 * This sample demonstrates how to create and send a mailing.
 * 
 * @author bgn Inxmail GmbH
 */
class Inx_Demo_MailingAttach extends Inx_Demo_Mailing
{
	/** Name of the file to be uploaded */
	const ATTACH_DEMO_FILE = "attach_demo.gif";

	/** Filename as seen by the recipient */	
	const ATTACHMENT_NAME = "demo.gif";
		
	/**
	 * Create a new mailing sample. 
	 */
	public function __construct( )
	{
	}

	/**
	 * Run the demonstration
	 * 
	 * @param string $sEmailAddress
	 * @param Inx_Api_Session $oSession
	 * @param string $sMailingListName
	 */
	public function runDemo( $sEmailAddress, Inx_Api_Session $oSession, $sMailingListName )
	{
		$this->_init($sEmailAddress, $oSession, $sMailingListName);
		$mailing = $this->createMailingDemo( self::CONTENT . $this->createAttachment( self::ATTACH_DEMO_FILE) );
		$this->sendTestMailDemo( $mailing, $sEmailAddress, $this->locateTestRecipient() );
	}

	
	/**
	 * Updload the attachment file, return the correct [%attach] tag for Inxmail.
	 * 
	 * @param string $sFilename
	 * @return string
	 */
	private function createAttachment( $sFilename )
	{
		echo "Uploading resource [$sFilename]...";
		
		$mgr = $this->_oSession->getResourceManager(); 
		
		if (! file_exists(self::ATTACH_DEMO_FILE)) {
			throw new Exception("File '" . self::ATTACH_DEMO_FILE ."' does not exist." );
		}
		$rsF = fopen(self::ATTACH_DEMO_FILE, 'rb');
        $res = $mgr->upload(null, self::ATTACHMENT_NAME, $rsF);
        fclose($rsF);
		
		echo "ok. Resource-Id = " . $res->getId() . "\n";
		
		return "[%attach(" . $res->getId() . "); " . $res->getName() . "]\n";
	}


	public function main( $args )
	{
		/* // For use with a proxy server:
		Inx_Api_Session::setProperty( "http.proxyHost", "- your proxy server IP address" );
		Inx_Api_Session::setProperty( "http.proxyPort", "8080" );
		Inx_Api_Session::setProperty( "http.proxyUser", "test" );
		Inx_Api_Session::setProperty( "http.proxyPassword", "test" );
		*/

		if( sizeof($args) != 5 )
		{
			echo  
				"Demonstrates creation and sending of mailings. To run this demo, please create\n" .
				"a standard mailing list. You need to have at least one recipient in the system\n" .
				"list, whose data is used for personalization. The created mailing will be sent\n" .
				"to an email address passed as parameter to this demo.\n\n" .
			    "Parameters: <server> <user> <password> <mailinglist> <your email address>\n" .
			    "           Example: http://127.0.0.1/inxmail supervisor inxmail TestList name@company.com\n";
			throw new Inx_Api_IllegalArgumentException( "Not enough parameters." );
		}
		
		echo "Logging in to " . $args[0] . "...\n";
		$s = Inx_Api_Session::createRemoteSession( $args[0], $args[1], $args[2] );
		
		$this->runDemo($args[4], $s, $args[3]);
		
		if( $s != null )
			$s->close();
	}
}