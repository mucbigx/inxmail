<?php



/**
 * This sample demonstrates following parts of the API:
 * 
 * <ul>
 * <li>how to create or find a standard mailing list</li>
 * <li>how add single recipients</li>
 * <li>the usage of the BulkChannel for adding multiple recipients</li>
 * <li>how to search/retrieve recipients and change their profile</li>
 * </ul>
 * 
 * @author bgn, Inxmail GmbH
 */
class Inx_Demo_Recipient
{
	/**
	 * Connected session
	 *
	 * @var Inx_Api_Session
	 */
	private $_oSession;

	
	public function __construct( )
	{
		
	}
	
	
	/**
	 * Run the demonstration
	 * 
	 * @throws Exception
	 */
	public function runDemo(Inx_Api_Session $oSession)
	{
		$this->_oSession = $oSession;
		$sUniqueName = microtime(true);
		$lc = $this->createMailingListDemo( "Recipient Demo" );
		
		// Add a single recipient
		$this->addRecipientDemo( $lc, $sUniqueName . "@inxmail-api.test" );
		
		// Add multiple recipients
		$this->addRecipientBulkDemo( $lc, array(
			$sUniqueName . "-1@inxmail-api.bulk",
			$sUniqueName . "-2@inxmail-api.bulk",
			$sUniqueName . "-3@inxmail-api.bulk") );
		
		// Search/Retrieve a recipient and change its email address
		$this->searchRetrieveDemo( $sUniqueName . "@inxmail-api.test", "changed-address@inxmail-api.test" );
	}


	/**
	 * Demonstrates how to create/select a standard mailing list
	 * 
	 * @param session           Connected session
	 * @param name              List name
	 * @return                  the list context
	 * @throws UpdateException
	 * @throws DataException
	 */
	private function createMailingListDemo( $name )
	{
		$lm = $this->_oSession->getListContextManager();
		$lc = $lm->findByName( $name );
		if( $lc==null )
		{
			$lc = $lm->createStandardList();
			$lc->updateName( $name );
			$lc->commitUpdate();
			
			echo "Created mailing list '" . $name . "' \n";
		}
		else
			echo "Found mailing list '" . $name . "' \n";
		
		return $lc;
	}
	
	
	/**
	 * Demonstrates how to add a single recipient to a mailing list
	 * 
	 * @param session
	 */
	private function addRecipientDemo( Inx_Api_List_ListContext $lc, $sEmailAddress )
	{
		$rm = $this->_oSession->createRecipientContext();
		$rmd = $rm->getMetaData();
		try
		{
			$rrs = $rm->select( null, null, null, $rmd->getEmailAttribute(), Inx_Api_Order::ASC );

			try
			{
				// Move to the "insert" row
				$rrs->moveToInsertRow();
				
				// Set the recipients email address
				$rrs->updateString( $rmd->getEmailAttribute(), $sEmailAddress );
				
				// Subscribe the recipient to the mailing list, otherwise it would show up
				// in the system list, only.
				$rrs->updateDatetime( $rmd->getSubscriptionAttribute( $lc ), date("c") );

				// This is how to set attribute data:
				// $rrs->updateBoolean( $rmd->getUserAttribute( "bool1" ), Boolean.TRUE );
				
				$rrs->commitRowUpdate();
				
				echo "Added recipient '" . $sEmailAddress . "' to list '" . $lc->getName() . "'";
			}
			catch( Exception $x )
			{
				echo $x->getTraceAsString();
			}

			$rrs->close();
		}
		catch( Inx_Api_Recipient_SelectException $x )
		{
			echo $x->getMessage();
			echo $x->getTraceAsString();
		}

		$rm->close();
	}
	
	
	/**
	 * Demonstrattes how to add multiple recipients using the batch channel. 
	 * 
	 * @param session        connected session
	 * @param $lc             list context
	 * @param $sEmailAddress   array of email addresses to add
	 * @throws AttributeNotFoundException
	 */
	private function addRecipientBulkDemo( Inx_Api_List_ListContext $lc, $aEmailAddress )
	{
		$rc = $this->_oSession->createRecipientContext();
		$rmd = $rc->getMetaData();
		$subscriptionAttribute = $rmd->getSubscriptionAttribute( $lc );
		
		$bc = $rc->createBatchChannel();
		for( $i=0; $i<sizeof($aEmailAddress); ++$i )
		{
			$bc->createRecipient( $aEmailAddress[$i], true );
			$bc->write( $subscriptionAttribute, date("c") );
		}
		$results = $bc->executeBatch();
		
		echo "Added " . sizeof($aEmailAddress) . "recipients.\n";
	}
	
	
	/**
	 * Demonstrates how to search for a recipient and change his/her profile information   
	 * 
	 * @param session        connected session
	 * @param emailPattern   search for the recipient using this pattern
	 * @param newEmail       change the first recipient found to this address
	 * @throws DataException on server-side exception
	 */
	private function searchRetrieveDemo( $sEmailPattern, $sNewEmail )
	{
		$rc = $this->_oSession->createRecipientContext();
		$sFilter = "email LIKE \"" . $sEmailPattern . "\"";
		$rrs = null;
		try
		{
			$rrs = $rc->select( null, null, $sFilter );
		}
		catch( Inx_Api_Recipient_SelectException $x )
		{
			echo $x->getTraceAsString();
			return; // Stop
		}
		
		if( $rrs->next() )
		{
			$rmd = $rc->getMetaData();
			
			$oldEmail = $rrs->getString( $rmd->getEmailAttribute() );
			$rrs->updateString( $rmd->getEmailAttribute(), $sNewEmail );
			
			try
			{
				$rrs->commitRowUpdate();
				echo "Email address changed from " . $oldEmail . " to " . $sNewEmail . "\n";
			}
			catch( Inx_Api_Recipient_BlackListException $x )
			{
				echo "Email address blacklisted (" . $sNewEmail . ")\n";
			}
			catch( Inx_Api_Recipient_IllegalValueException $x )
			{
				echo "Cannot set email address to " . $sNewEmail . "\n";
			}
			catch( Inx_Api_Recipient_DuplicateKeyException $x )
			{
				echo "Email address already used by another recipient (" . $sNewEmail . ")\n";
			}
			
		}
		else
		{
			echo "Recipient not found for pattern " . $sEmailPattern ;
		}
		
		$rrs->close();
	}
	

	public function main( $args )
	{
		/* // For use with a proxy server:
		Inx_Api_Session::setProperty( "http.proxyHost", "- your proxy server IP address" );
		Inx_Api_Session::setProperty( "http.proxyPort", "8080" );
		Inx_Api_Session::setProperty( "http.nonProxyHosts", "localhost|127.0.0.1" );
		Inx_Api_Session::setProperty( "http.proxyUser", "test" );
		Inx_Api_Session::setProperty( "http.proxyPassword", "test" );
		*/

		if( sizeof($args) != 3 )
		{
			echo "Missing parameters: <server> <username> <password> \n";
			echo "           Example: http://127.0.0.1/inxmail supervisor inxmail\n\n";
			throw new Inx_Api_IllegalArgumentException( "Not enough parameters." );
		}
		
		echo "Logging in to " . $args[0] . "...\n";
		$s = Inx_Api_Session::createRemoteSession( $args[0], $args[1], $args[2] );
		
		$this->runDemo($s);
		
		if( $s != null )
				$s->close();
	}
}
