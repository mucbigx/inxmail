<?php
class Inx_Demo_SubscriptionManager
{
    /**
     * Connected session
     *
     * @var Inx_Api_Session
     */
    private $_oSession;
    
    /**
     * The start date as ISO-8601 formatted datetime string.
     * 
     * @var string
     */
    private $_sStart;
    
    public function __construct( )
    {

    }
    
    /**
     * Run the demonstration
     * 
     * @throws Exception
     */
    public function runDemo(Inx_Api_Session $oSession)
    {
           $this->_oSession = $oSession;
           $sUniqueName = microtime(true);
           $lc = $this->createMailingListDemo( "Recipient Demo" );

           // Set start date before (un)subscriptions - ISO-8601 datetime format
           $this->_sStart = date( 'c' );
           
           // Subscribe a single recipient
           $this->subscribeRecipientDemo( $lc, $sUniqueName . "@inxmail-api.test" );
           
           // Unsubscribe a single recipient
           $this->unsubscribeRecipientDemo( $lc, $sUniqueName . "@inxmail-api.test" );
           
		   echo "Waiting 3 minutes to assure log entries are already written... \n";
		   sleep(180);
		   
           // Find the subscription log entries for all operations
           $this->findSubscriptionLogEntries( $lc );
    }
    
    /**
     * Demonstrates how to create/select a standard mailing list
     * 
     * @param name              List name
     * @return                  the list context
     * @throws UpdateException
     * @throws DataException
     */
    private function createMailingListDemo( $name )
    {
            $lm = $this->_oSession->getListContextManager();
            $lc = $lm->findByName( $name );
            if( $lc==null )
            {
                    $lc = $lm->createStandardList();
                    $lc->updateName( $name );
                    $lc->commitUpdate();  

                    echo "Created mailing list '" . $name . "' \n";
            }
            else
                    echo "Found mailing list '" . $name . "' \n"; 

            return $lc;
    }
    
    /**
     * Demonstrates how to subscribe a single recipient to a mailing list
     * 
     * @param Inx_Api_List_ListContext $lc
     * @param string $sEmailAddress
     */
    private function subscribeRecipientDemo( Inx_Api_List_ListContext $lc, $sEmailAddress )
    {
            // Attribute data can be set using an associative array where the key is the attribute name
            $attributes = array();            
            // $attributes['bool1'] = true;
            // $attributes['Firstname'] = 'John';
            // $attributes['Lastname'] = 'Doe';
        
            $sm = $this->_oSession->getSubscriptionManager();
            
            try
            {
                    $ret = $sm->processSubscription('api-demo', 'localhost', $lc, $sEmailAddress, $attributes);
                    
                    switch( $ret ) 
                    {
                        case Inx_Api_Subscription_SubscriptionManager::PROCESS_ACTIVATION_SUCCESSFULLY:
                            echo "Subscribed recipient '" . $sEmailAddress . "' to list '" . $lc->getName() . "' \n";
                            break;
                        case Inx_Api_Subscription_SubscriptionManager::PROCESS_ACTIVATION_FAILED_ADDRESS_ILLEGAL:
                            echo "Could not subscribe recipient '" .$sEmailAddress . "' - address probably invalid \n";
                            break;
                        
                        // Just in case the result is not well defined
                        default:
                            echo "Unknown result for recipient '" .$sEmailAddress . "': " . $ret . " \n";
                    }
            }
            catch( Inx_Api_FeatureNotAvailableException $x )
            {
                    // In this case, the subscription manager agent is not configured for the given list.
                    echo $x->getMessage();
                    echo $x->getTraceAsString();
            }
            catch( Exception $x )
            {
                    // Any other execption, including Inx_Api_SecurityException.
                    echo $x->getMessage();
                    echo $x->getTraceAsString();
            }
    }
    
    /**
     * Demonstrates how to unsubscribe a single recipient from a mailing list
     * 
     * @param Inx_Api_List_ListContext $lc
     * @param string $sEmailAddress
     */
    private function unsubscribeRecipientDemo( Inx_Api_List_ListContext $lc, $sEmailAddress )
    {
        // Attribute data can be set using an associative array where the key is the attribute name
        $attributes = array();            
        // $attributes['bool1'] = false;
        
        $sm = $this->_oSession->getSubscriptionManager();
        
        // The source identifier is used to determine where the request came from.
        $sSourceIdentifier = 'api-demo';
        
        // The remote address is the IP address or DNS hostname of the entity that requested the unsubscription.
        $sRemoteAddress = 'localhost';
        
        // The mailing ID identifies the mailing containing the unsubscription link. Use 0 if no such mailing exists.
        $iMailingId = 0;
        
        try
        {
            // This is the preferred method, in case you know the mailing reference. The "mailref" is a string which identifies 
            // a mailing sent to a specific recipient. It can be created in a mailing using the [%mailref] tag and is also part
            // of the string produced by the [%online_params] tag.
            // 
            // $ret = $sm->processUnsubscription4($sSourceIdentifier, $sRemoteAddress, $lc, 
            //      $sEmailAddress, $sMailingRef, $attributes);

            // In case you do not know the mailing reference, use the mailing ID instead. If you do not know this either or the 
            // unsubscription is not mailing specific, use 0 to indicate this fact.
            $ret = $sm->processUnsubscription3($sSourceIdentifier, $sRemoteAddress, $lc, 
                    $sEmailAddress, $iMailingId, $attributes);
            
            switch( $ret ) 
            {
                case Inx_Api_Subscription_SubscriptionManager::PROCESS_ACTIVATION_SUCCESSFULLY:
                    echo "Unsubscribed recipient '" . $sEmailAddress . "' from list '" . $lc->getName() . "' \n";
                    break;
                case Inx_Api_Subscription_SubscriptionManager::PROCESS_ACTIVATION_FAILED_ADDRESS_ILLEGAL:
                    echo "Could not unsubscribe recipient '" .$sEmailAddress . "' - address probably invalid \n";
                    break;

                // Just in case the result is not well defined
                default:
                    echo "Unknown result for recipient '" .$sEmailAddress . "': " . $ret . " \n";
            }
        }
        catch( Inx_Api_FeatureNotAvailableException $x )
        {
                // In this case, the subscription manager agent is not configured for the given list.
                echo $x->getMessage();
                echo $x->getTraceAsString();
        }
        catch( Exception $x )
        {
                // Any other execption, including Inx_Api_SecurityException.
                echo $x->getMessage();
                echo $x->getTraceAsString();
        }
    }
    
    /**
     * Demonstrates how to find subscrition log entries
     * 
     * @param Inx_Api_List_ListContext $lc
     */
    private function findSubscriptionLogEntries( Inx_Api_List_ListContext $lc )
    {
        $rc = $this->_oSession->createRecipientContext();
        // Find the email attribute
        $emailAttr = $rc->getMetaData()->getEmailAttribute();
        // This is how you provide the attributes you wish to fetch
        $attrs = array( $emailAttr );
        
        $sm = $this->_oSession->getSubscriptionManager();
        
        // Find all log entries between the given dates for the specified list
        $rowSet = $sm->getLogEntriesBetweenAndList($lc, $this->_sStart, date('c'), $rc, $attrs);
        
        try
        {
            // Print out the log entries, briefly.
            while( $rowSet->next() )
            {
                $emailAddress = $rowSet->getEmailAddress();
                $list = $rowSet->getListId();
                $type = $rowSet->getType();

                echo 'Recipient: ' . $emailAddress . ' List: ' . $list . ' Type: ' . $type . " \n";
            }
        }
        catch( Inx_Api_DataException $x )
        {
            // Entry no longer exists
            echo $x->getMessage();
            echo $x->getTraceAsString();
        }
        catch( Exception $x )
        {
            // Any other execption
            echo $x->getMessage();
            echo $x->getTraceAsString();
        }
        
        // Close the no longer used resources
        $rowSet->close();     
        $rc->close();
    }
    
    public function main( $args )
    {
            /* // For use with a proxy server:
            Inx_Api_Session::setProperty( "http.proxyHost", "- your proxy server IP address" );
            Inx_Api_Session::setProperty( "http.proxyPort", "8080" );
            Inx_Api_Session::setProperty( "http.nonProxyHosts", "localhost|127.0.0.1" );
            Inx_Api_Session::setProperty( "http.proxyUser", "test" );
            Inx_Api_Session::setProperty( "http.proxyPassword", "test" );
            */

            if( sizeof($args) != 3 )
            {
                    echo "Missing parameters: <server> <username> <password> \n";
                    echo "           Example: http://127.0.0.1/inxmail supervisor inxmail\n\n";
                    throw new Inx_Api_IllegalArgumentException( "Not enough parameters." );
            }

            echo "Logging in to " . $args[0] . "...\n";
            $s = Inx_Api_Session::createRemoteSession( $args[0], $args[1], $args[2] );

            $this->runDemo($s);

            if( $s != null )
                    $s->close();
    }
}
?>
