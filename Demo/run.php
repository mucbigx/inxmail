<?php
class Demo_Run
{
	public static function run ()
	{
		if (! empty($GLOBALS['argv'][1])) {
			$class_name = 'Inx_Demo_' . $GLOBALS['argv'][1];
			$args = array();
			
			foreach ($GLOBALS['argv'] as $i=>$value) {
				if ($i>1 && !empty($value)) {
					$args[] = $GLOBALS['argv'][$i];
				}
			}
			try {
				$demo = new $class_name();
				$demo->main($args);
			} catch(Exception $e) {
				echo "\n\nDemo has failed.\n\n" . $e->getMessage() . "\n";
				exit;
			}
		} else {
			echo "Missing parameters: <Demo name> <server> <username> <password> \n";
			echo "           Example: List http://127.0.0.1/inxmail supervisor inxmail\n";
		}
	}
}
date_default_timezone_set('UTC');

foreach (glob('*.php') as $filename)
{
	if($filename != basename(__FILE__))
    	require_once $filename;
}

require_once dirname(dirname(__FILE__)).'../inxmail_api/Apiimpl/Loader.php';
Inx_Apiimpl_Loader::registerAutoload();

Demo_Run::run();